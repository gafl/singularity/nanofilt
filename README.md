# nanofilt Singularity container
Bionformatics package nanofilt<br>
Filtering and trimming of Oxford Nanopore Sequencing data
https://github.com/wdecoster/nanofilt
nanofilt Version: 2.6.0<br>

Singularity container based on the recipe: Singularity.nanofilt_v2.6.0

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
singularity pull nanofilt_v2.6.0.sif oras://registry.forgemia.inra.fr/gafl/singularity/nanofilt/nanofilt:latest


